﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.ReplyMarkups;

namespace ConverterBot
{
    class Program
    {
        static readonly string botKey = "1532103261:AAFUiEyfKrmZ0vwomCSGWl3L6UnOgD29gs4";
        static ITelegramBotClient botClient;
        static List<Valute> Valutes;
        static string OutDate = "";

        static void Main()
        {
            Valutes = new List<Valute>();

            botClient = new TelegramBotClient(botKey);
            var me = botClient.GetMeAsync().Result;
            botClient.OnMessage += Bot_OnMessage;
            botClient.StartReceiving();
            Console.ReadLine();
            botClient.StopReceiving();
        }
        static async void Bot_OnMessage(object sender, MessageEventArgs e)
        {
            var message = e.Message;
            if (message == null)
                return;

            string text =
@"Список команд:
/start - запуск бота
/help - список команд
/valutes - ближайший курс валют

Чтобы конвертировать из одной валюты в другую введите следующую конструкцию(100,25 RUB USD):
Число ТрехбуквенныйКодВалюты ИнтересующаяВалюта";
            switch (message.Text)
            {
                case "/start":
                    await botClient.SendTextMessageAsync(message.From.Id, text);
                    break;
                case "/help":
                    await botClient.SendTextMessageAsync(message.From.Id, text);
                    break;
                case "/valutes":
                    LoadInfo();
                    string answer = "Ближайший курс валюты за " + $"{OutDate}\n";
                    foreach (Valute valute in Valutes)
                        answer += valute.Nominal + " " + valute.CharCode + " - " + Math.Round(valute.Value, 2) + "RUB\n";
                    await botClient.SendTextMessageAsync(chatId: message.Chat, text: answer);
                    break;
                default:
                    string[] result;
                    result = message.Text.Split(' ');
                    if(result.Length != 3)
                    {
                        await botClient.SendTextMessageAsync(chatId: message.Chat, text: "Произошла ошибка. Возможно вы что-то ввели неправильно");
                        break;
                    }
                    Valute tempValute1 = null, tempValute2 = null;
                    LoadInfo();
                    result[1] = result[1].ToUpper();
                    result[2] = result[2].ToUpper();

                    foreach (Valute valute in Valutes)
                    {
                        if (valute.CharCode.Equals(result[1])) tempValute1 = valute;
                        if (valute.CharCode.Equals(result[2])) tempValute2 = valute;
                    }
                    if(result[2].Equals("RUB")) { tempValute2 = new Valute(); tempValute2.Nominal = 1; tempValute2.Value = 1; tempValute2.CharCode = "RUB"; }
                    if (result[1].Equals("RUB")) { tempValute1 = new Valute(); tempValute1.Nominal = 1; tempValute1.Value = 1; tempValute1.CharCode = "RUB"; }
                    if (tempValute1 == null || tempValute2 == null)
                    {
                        await botClient.SendTextMessageAsync(chatId: message.Chat, text: "Произошла ошибка. Возможно вы что-то ввели неправильно");
                        break;
                    }

                    double value;
                    if (!Double.TryParse(result[0], out value))
                    {
                        await botClient.SendTextMessageAsync(chatId: message.Chat, text: "Введите число через запятую");
                        break;
                    }
                    double res = tempValute1.Value * value / (tempValute2.Value * tempValute1.Nominal) * tempValute2.Nominal;
                    res = Math.Round(res, 2);
                    await botClient.SendTextMessageAsync(chatId: message.Chat, text: "По курсу за " + $"{OutDate}: " + res.ToString() + tempValute2.CharCode);
                    break;
            }
        }
        
        static void LoadInfo()
        {
            try
            {
                HttpClient client = new HttpClient();
                var response = client.GetAsync($"https://www.cbr-xml-daily.ru/daily_json.js").Result;

                Valutes.Clear();
                var content = response.Content.ReadAsStringAsync();
                JObject jObject = JObject.Parse(content.Result);
                var valutes = jObject.SelectToken(@"$.Valute").Values();
                OutDate = jObject.SelectToken(@"$.Date").ToString();
                OutDate = (OutDate.Split(' '))[0];
                foreach (var temp in valutes)
                {
                    Valute valute = JsonConvert.DeserializeObject<Valute>(temp.ToString());
                    Valutes.Add(valute);
                }
            }
            catch (Exception ex)
            { }
        }
    }
}
